"use strict";

const Hapi = require("hapi");
const inert = require("inert");
const vision = require("vision");
const _ = require("lodash");
const chalk = require("chalk");
const Handlebars = require("handlebars");
const routes = require("./server/routes");
// const MongoDB = require('hapi-mongodb');
// const DBConfig = require('./server/config/DbConfig');


const launchServer = async function() {
  const server = new Hapi.Server({ port: 4001, host: '0.0.0.0' });

  //server.connection({host: "0.0.0.0", port: process.env.port || 4000});

  await server.register(inert);
  await server.register(vision);
//   await server.register({
//     plugin: require('hapi-mongodb'),
//     options: DBConfig.opts
//   });

  server.route(routes);

  server.views({
    engines: {
        hbs: { module: Handlebars.create() }
    },
    path: `${__dirname}/server/views`,
    partialsPath: `${__dirname}/server/views/partials`
    //helpersPath: `${__dirname}/server/views/helpers`,
  });

  await server.start();
  console.log(`Server started at ${server.info.uri}`);  
}

launchServer().catch((err) => {
  console.error(err);
  process.exit(1);
});


