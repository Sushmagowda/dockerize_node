"use strict";

const rootHandler = (request, reply) => {
  return reply.view("index");
}

module.exports = rootHandler;
