"use strict";

const addNewUserHandler = async (request, reply) => {
    const db = request.mongo.db;
    const ObjectID = request.mongo.ObjectID;
    let userData = {
        firstName: request.payload.Firstname,
        lastName: request.payload.Lastname,
        phone: request.payload.phone,
        email: request.payload.email
    };

    try {
        await db.collection("users").save(userData);
        return "User data saved successfully";
    }
    catch (err) {
        return `Error while saving user data ${err}`;
    }
}

module.exports = addNewUserHandler;